import cx from 'classnames';
import { BigNumber } from 'ethers';
import React, { LegacyRef } from 'react';

import Svg from 'components/core/Svg/Svg';
import { IS_INITIAL_LOAD_DONE } from 'constants/dataAttributes';
import { navigationTabs as navigationTabsDefault } from 'constants/navigationTabs/navigationTabs';
import { NavigationTab } from 'constants/navigationTabs/types';
import i18n from 'i18n';
import { allocateWithNumber } from 'svg/navigation';
import getFormattedEthValue from 'utils/getFormattedEthValue';

import styles from './MainLayout.module.scss';

export function getIndividualRewardText({
  individualReward,
  currentEpoch,
}: {
  currentEpoch?: number;
  individualReward?: BigNumber;
}): string {
  if (currentEpoch !== undefined && currentEpoch === 1) {
    return i18n.t('layouts.main.noRewardBudgetYet');
  }
  if (currentEpoch === undefined || individualReward === undefined) {
    return i18n.t('layouts.main.loadingRewardBudget');
  }
  if (individualReward.isZero()) {
    return i18n.t('layouts.main.noRewardBudgetYet');
  }
  return i18n.t('common.budget', {
    budget: getFormattedEthValue(individualReward).fullString,
  });
}

export function getNavigationTabsWithAllocations(
  idsInAllocation: string[] | undefined,
  isAllocationValueChanging: boolean,
  numberOfAllocationsRef?: LegacyRef<HTMLDivElement> | undefined,
  navigationTabs = navigationTabsDefault,
): NavigationTab[] {
  const dataAttributes = {
    [IS_INITIAL_LOAD_DONE]: 'false',
  };
  const newNavigationTabs = [...navigationTabs];
  newNavigationTabs[1] =
    idsInAllocation && idsInAllocation.length > 0
      ? {
          ...newNavigationTabs[1],
          iconWrapped: (
            <div className={styles.iconNumberOfAllocations}>
              <div
                ref={numberOfAllocationsRef}
                className={cx(
                  styles.numberOfAllocations,
                  isAllocationValueChanging && styles.isAllocationValueChanging,
                )}
                data-test="MainLayout__navigation__numberOfAllocations"
                {...dataAttributes}
              >
                {idsInAllocation.length}
              </div>
              <Svg img={allocateWithNumber} size={3.2} />
            </div>
          ),
        }
      : newNavigationTabs[1];
  return newNavigationTabs;
}
