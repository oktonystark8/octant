export default interface ButtonAddToAllocateProps {
  className?: string;
  dataTest?: string;
  isAlreadyAdded: boolean;
  onClick: () => void;
}
