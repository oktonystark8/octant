export default interface ProposalLoadingStatesProps {
  isLoading: boolean;
  isLoadingError: boolean;
}
