export default interface AllocationInfoBoxesProps {
  classNameBox: string;
  isConnected: boolean;
  isDecisionWindowOpen: boolean;
}
