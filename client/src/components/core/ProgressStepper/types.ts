export default interface ProgressStepperProps {
  currentStepIndex: number;
  steps: string[];
}
