import { SvgImageConfig } from 'components/core/Svg/types';

export const arrowTopRight: SvgImageConfig = {
  markup:
    '<path stroke="#2D9B87" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.434 1.7h6.441v6.528"/><path stroke="#2D9B87" stroke-linecap="round" stroke-width="2" d="M1-1h9.118" transform="matrix(-.69444 .71955 -.71011 -.70409 8.596 1)"/>',
  viewBox: '0 0 10 10',
};

export const arrowRight: SvgImageConfig = {
  markup:
    '<path fill="#171717" d="m7.99 13.253-1.278-1.264 4.439-4.44H0V5.704h11.15L6.713 1.271 7.99 0l6.627 6.627-6.627 6.626Z"/>',
  viewBox: '0 0 15 14',
};

export const chevronBottom: SvgImageConfig = {
  markup:
    '<path d="M7.18198 1.68198L3.99823 4.86573L0.816245 1.68375" stroke="#171717" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>',
  viewBox: '0 0 8 6',
};

export const tick: SvgImageConfig = {
  markup:
    '<path stroke="#271558" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1.407 6.664 2.938 3.752 8.485-8.485"/>',
  viewBox: '0 0 14 12',
};

export const cross: SvgImageConfig = {
  markup:
    '<path stroke="#271558" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 9.485 9.485 1M9.485 9.485 1 1"/>',
  viewBox: '0 0 11 11',
};

export const heart: SvgImageConfig = {
  markup:
    '<path stroke="#9EA39E" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M7 11.5 1.886 6.164a3.027 3.027 0 0 1 3.07-5.019c.455.148.87.401 1.21.74L7 2.721l.835-.835a3.026 3.026 0 0 1 4.28 4.28L7 11.498Z"/>',
  viewBox: '0 0 14 13',
};

export const plus: SvgImageConfig = {
  markup:
    '<path stroke="#EBEBEB" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1.344 7.657h12M7.344 13.657v-12"/>',
  viewBox: '0 0 15 15',
};

export const minus: SvgImageConfig = {
  markup:
    '<path stroke="#EBEBEB" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1.001h12"/>',
  viewBox: '0 0 14 2',
};

export const notificationIconWarning: SvgImageConfig = {
  markup:
    '<path fill="#5B8A81" d="M16.05 2.12a.902.902 0 0 1 .9 0l11.1 6.408c.278.161.45.46.45.781v12.816c0 .322-.172.62-.45.78l-11.1 6.408a.902.902 0 0 1-.9 0l-11.1-6.408a.902.902 0 0 1-.45-.78V9.309c0-.322.172-.62.45-.78l11.1-6.408Z"/><path fill="#fff" d="M15.55 18.53h2.058l.32-5.27V9.5H15.23v3.76l.319 5.27ZM16.579 23c.87 0 1.596-.71 1.596-1.561 0-.87-.727-1.597-1.596-1.597-.87 0-1.579.728-1.579 1.597 0 .851.71 1.561 1.579 1.561Z"/>',
  viewBox: '0 0 33 32',
};

export const notificationIconSuccess: SvgImageConfig = {
  markup:
    '<path fill="#5B8A81" d="M15.55 2.12a.902.902 0 0 1 .9 0l11.1 6.408c.278.161.45.46.45.781v12.816c0 .322-.172.62-.45.78l-11.1 6.408a.902.902 0 0 1-.9 0l-11.1-6.408a.902.902 0 0 1-.45-.78V9.309c0-.322.172-.62.45-.78l11.1-6.408Z"/><path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m11.34 15.732 2.447 3.127 7.072-7.072"/>',
  viewBox: '0 0 33 32',
};

export const questionMark: SvgImageConfig = {
  markup:
    '<path stroke="#9EA39E" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M7 7a2 2 0 1 1 2.667 1.886A1 1 0 0 0 9 9.83v.671"/><circle cx="9" cy="12.75" r="1" fill="#9EA39E"/><path stroke="#9EA39E" stroke-miterlimit="10" stroke-width="1.5" d="M9 16.5a7.5 7.5 0 1 0 0-15 7.5 7.5 0 0 0 0 15Z"/>',
  viewBox: '0 0 18 18',
};

export const info: SvgImageConfig = {
  markup:
    '<circle cx="16" cy="16" r="11.5" stroke="#EBEBEB"/><path fill="#171717" d="M16 8.5a7.5 7.5 0 1 1 0 15 7.5 7.5 0 0 1 0-15Z"/><path fill="#fff" d="M17.5 19H17a1 1 0 0 1-1-1v-2.5a.5.5 0 0 0-.5-.5H15"/><path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M17.5 19H17a1 1 0 0 1-1-1v-2.5a.5.5 0 0 0-.5-.5H15"/><circle cx="1" cy="1" r="1" fill="#fff" transform="matrix(1 0 0 -1 14.5 13.25)"/>',
  viewBox: '0 0 32 32',
};
