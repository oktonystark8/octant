import { SvgImageConfig } from 'components/core/Svg/types';

import styles from './style.module.scss';

export const allocate: SvgImageConfig = {
  className: styles.allocate,
  markup:
    '<g stroke="#CDD1CD" clip-path="url(#allocate)"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12.249 19.752h-6a1.5 1.5 0 0 1-1.5-1.5v-12a1.5 1.5 0 0 1 1.5-1.5h18a1.5 1.5 0 0 1 1.5 1.5V13"/><circle cx="9.875" cy="9.406" r="1.875" stroke-width="1.5"/><path stroke-linecap="round" d="M15 7.5h7M15 9.5h5M15 11.5h3.625"/><path stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M27.25 21.252a6 6 0 1 0-12 0 6 6 0 0 0 12 0ZM21.25 24.252v-6M21.25 18.252l2.25 2.25M21.25 18.252 19 20.502"/></g><defs><clipPath id="allocate"><path fill="#fff" d="M4 4h24v24H4z"/></clipPath></defs>',
  viewBox: '0 0 32 32',
};

export const allocateWithNumber: SvgImageConfig = {
  className: styles.allocateWithNumber,
  markup:
    '<path fill="#FF6157" d="M27.997 21.251a6.749 6.749 0 1 0-13.498 0 6.749 6.749 0 0 0 13.498 0Z"/><path stroke="#FF6157" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M12.249 19.752h-6a1.5 1.5 0 0 1-1.5-1.5v-12a1.5 1.5 0 0 1 1.5-1.5h18a1.5 1.5 0 0 1 1.5 1.5V13"/><circle cx="9.875" cy="9.405" r="1.875" stroke="#FF6157" stroke-width="1.5"/><path stroke="#FF6157" stroke-linecap="round" d="M14.999 7.5h7M14.999 9.5h5M14.999 11.5h3.63"/>',
  viewBox: '0 0 32 32',
};

export const metrics: SvgImageConfig = {
  markup:
    '<g stroke="#CDD1CD" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" clip-path="url(#metrics)"><path d="M16 27.25c6.213 0 11.25-5.037 11.25-11.25S22.213 4.75 16 4.75 4.75 9.787 4.75 16 9.787 27.25 16 27.25Z"/><path d="M21.25 21.25h-10.5V11.5"/><path d="m10.75 17.5 3-2.187a.377.377 0 0 1 .553.164l2.531 2.545a.375.375 0 0 0 .666.046L19 15.08a.372.372 0 0 1 .231-.171l2.1-.409"/></g><defs><clipPath id="metrics"><path fill="#fff" d="M4 4h24v24H4z"/></clipPath></defs>',
  viewBox: '0 0 32 32',
};

export const proposals: SvgImageConfig = {
  markup:
    '<rect width="16" height="4" x="5.5" y="7" stroke="#CDD1CD" stroke-linejoin="round" stroke-width="1.5" rx="2"/><rect width="16" height="4" x="5.5" y="14" stroke="#CDD1CD" stroke-linejoin="round" stroke-width="1.5" rx="2"/><rect width="16" height="4" x="5.5" y="21" stroke="#CDD1CD" stroke-linejoin="round" stroke-width="1.5" rx="2"/><rect width="4" height="4" x="24.5" y="7" stroke="#CDD1CD" stroke-linejoin="round" stroke-width="1.5" rx="2"/><rect width="4" height="4" x="24.5" y="14" stroke="#CDD1CD" stroke-linejoin="round" stroke-width="1.5" rx="2"/><rect width="4" height="4" x="24.5" y="21" stroke="#CDD1CD" stroke-linejoin="round" stroke-width="1.5" rx="2"/>',
  viewBox: '0 0 32 32',
};

export const settings: SvgImageConfig = {
  markup:
    '<path stroke="#CDD1CD" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M22.376 14.5H9.626a4.888 4.888 0 0 1-4.875-4.875v0A4.889 4.889 0 0 1 9.626 4.75h12.75a4.89 4.89 0 0 1 4.875 4.875v0a4.889 4.889 0 0 1-4.875 4.875v0ZM22.376 27.25H9.626a4.888 4.888 0 0 1-4.875-4.875v0A4.89 4.89 0 0 1 9.626 17.5h12.75a4.89 4.89 0 0 1 4.875 4.875v0a4.889 4.889 0 0 1-4.875 4.875v0Z" clip-rule="evenodd"/><path stroke="#CDD1CD" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M9.626 11.5a1.875 1.875 0 1 0 0-3.75 1.875 1.875 0 0 0 0 3.75ZM22.376 24.25a1.875 1.875 0 1 0 0-3.75 1.875 1.875 0 0 0 0 3.75Z"/>',
  viewBox: '0 0 32 32',
};

export const earn: SvgImageConfig = {
  markup:
    '<g clip-path="url(#earn)"><path stroke="#CDD1CD" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M25.438 21.5v2.75a1.375 1.375 0 0 1-1.375 1.375H8.25a2.062 2.062 0 0 1-2.063-2.063V8.438A2.063 2.063 0 0 1 8.25 6.376h14.438a1.375 1.375 0 0 1 1.375 1.375v2.063"/><path stroke="#CDD1CD" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M25.438 21.5a1.375 1.375 0 0 0 1.375-1.375V16a1.375 1.375 0 0 0-1.375-1.375H22a3.438 3.438 0 0 0 0 6.875h3.438Z"/><circle cx="21.945" cy="18.063" r="1" fill="#CDD1CD"/><path stroke="#CDD1CD" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M25.438 14.625v-3.438a1.375 1.375 0 0 0-1.375-1.375H10.655a1.61 1.61 0 0 1-1.48-.82"/></g><defs><clipPath id="earn"><path fill="#fff" d="M5.5 5h22v22h-22z"/></clipPath></defs>',
  viewBox: '0 0 32 32',
};

export const chevronLeft: SvgImageConfig = {
  markup:
    '<path stroke="#CDD1CD" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M18.515 24.485 10.029 16l8.486-8.485"/>',
  viewBox: '0 0 32 32',
};
