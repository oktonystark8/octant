export const floatNumberWithUpTo2DecimalPlaces = /^\d+\.?(\d{1,2})?$|^\d+$/;

export const floatNumberWithUpTo18DecimalPlaces = /^\d+\.?(\d{1,18})?$|^\d+$/;

export const numbersOnly = /^[0-9]*$/;

export const dotAndZeroes = /\.?0+$/;

export const comma = /,/;
