// ----------------
// CONTRACT NAMES
// ----------------
export const AUTH = 'Auth';
export const PROPOSALS = 'Proposals';
export const DEPOSITS = 'Deposits';
export const TRACKER = 'Tracker';
export const TRACKER_WRAPPER = 'TrackerWrapper';
export const REWARDS = 'Rewards';
export const TOKEN = 'Token';
export const FAUCET = 'TestGLMFaucet';
export const ALLOCATIONS = 'Allocations';
export const ALLOCATIONS_STORAGE = 'AllocationsStorage';
export const EPOCHS = 'Epochs';
export const OCTANT_ORACLE = 'OctantOracle';
export const PAYOUTS = 'Payouts';
export const PAYOUTS_MANAGER = 'PayoutsManager';
export const WITHDRAWALS_TARGET = 'WithdrawalsTarget';
